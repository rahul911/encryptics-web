import React from 'react';
import Background from 'views/background';
import CenteredPane from 'views/centeredpane';
import 'views/styles/dashboard.scss';  

function Dashboard(props) {
    return (
        <>
            <Background className='dashboard'>
                <CenteredPane className='false-ceiling'>
                </CenteredPane>
            </Background>
        </>
    );
}

export default Dashboard;